create table role (
	role_id int not null auto_increment primary key
	, name varchar(50) not null unique
);

create table resource_descriptor (
	resource_id int not null auto_increment primary key
	, url varchar(50) not null unique
);

create table role_resouece (
	resource_id int not null
	, role_id int not null
	, primary key (resource_id, role_id)
	, FOREIGN KEY role_resource_resource_fk (resource_id) references resource_descriptor (resource_id)
	, FOREIGN KEY role_resource_role_fk (role_id) references role (role_id)
);

