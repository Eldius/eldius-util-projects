package net.eldiosantos.util.filecompressor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import net.eldiosantos.util.file.tools.FileDataTransfer;

public class GZipFileCompressor implements FileCompressor {

	/* (non-Javadoc)
	 * @see net.eldiosantos.util.filecompressor.FileCompressor#compressFile(java.io.InputStream, java.io.File)
	 */
	public void compressFile(InputStream in, File outputFile) throws FileNotFoundException, IOException{

		OutputStream out = new GZIPOutputStream(new FileOutputStream(outputFile));

		new FileDataTransfer().transferDataToOtherStream(in, out);
		
		out.flush();
		out.close();
	}

	/* (non-Javadoc)
	 * @see net.eldiosantos.util.filecompressor.FileCompressor#decompressFile(java.io.File, java.io.File)
	 */
	public void decompressFile(File compressedFile, File outputFile) throws FileNotFoundException, IOException{


		InputStream in = new GZIPInputStream(new FileInputStream(compressedFile));

		OutputStream out = new FileOutputStream(outputFile);

		new FileDataTransfer().transferDataToOtherStream(in, out);
	}

	public void compressFile(File in, File outputFile)
			throws FileNotFoundException, IOException {

		this.compressFile(new FileInputStream(in), outputFile);
	}
}
