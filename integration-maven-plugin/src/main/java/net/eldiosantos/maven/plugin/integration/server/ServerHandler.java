package net.eldiosantos.maven.plugin.integration.server;

import net.eldiosantos.maven.plugin.integration.server.dsl.interfaces.ContextPathConfigurator;
import net.eldiosantos.maven.plugin.integration.server.dsl.interfaces.DatasourceConfigurator;
import net.eldiosantos.maven.plugin.integration.server.dsl.interfaces.PortConfigurator;
import net.eldiosantos.maven.plugin.integration.server.dsl.interfaces.ServerHandlerBuilder;




public class ServerHandler implements PortConfigurator, ContextPathConfigurator, DatasourceConfigurator, ServerHandlerBuilder {

	private Integer port = null;

	private String dataSourceFile = null;
	
	private String contextPath = null;

	
	
	public ServerHandler(Integer port, String dataSourceFile, String contextPath) {
		super();
		this.port = port;
		this.dataSourceFile = dataSourceFile;
		this.contextPath = contextPath;
	}

	private ServerHandler(){
		
	}

	public static PortConfigurator initBuildProcess(){
		return new ServerHandler();
	}

	@Override
	public ServerHandler build() {
		return this;
	}

	@Override
	public ServerHandlerBuilder setDatasourceFilePath(String dataSourceFile) {
		if(dataSourceFile!=null){
			this.dataSourceFile = dataSourceFile;
		}else{
			throw new IllegalArgumentException("Datasource path is null. If you don't want a datasource, please call skipDatasourceConfiguration method.");
		}
		return this;
	}

	@Override
	public ServerHandlerBuilder skipDatasourceConfiguration() {
		return this;
	}

	@Override
	public DatasourceConfigurator setContextPath(String contextPath) {
		this.contextPath = contextPath;
		return this;
	}

	@Override
	public ContextPathConfigurator setPort(Integer port) {
		if(this.port != null){
			this.port = port;
		}else{
			throw new IllegalArgumentException("Port number must be not null.");
		}
		return this;
	}
}
