package net.eldiosantos.maven.plugin.integration.server.dsl.interfaces;

public interface ContextPathConfigurator {

	public abstract DatasourceConfigurator setContextPath(String contextPath);

}