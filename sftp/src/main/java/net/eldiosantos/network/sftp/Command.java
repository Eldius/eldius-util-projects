package net.eldiosantos.network.sftp;

import java.io.IOException;

import javax.swing.JOptionPane;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

public class Command {

	/**
	 * @param args
	 * @throws JSchException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws JSchException, IOException {
		// TODO Auto-generated method stub
		
		Proxy1 p = new Proxy1();
		
		p.setHTTPHost("10.42.0.21");
		p.setHTTPPort(9090);
		p.setHTTPUsername("eldio.junior");
		p.setHTTPPassword("Senha123");
		
		p.setHttpProxy();
		
		JSch jsch=new JSch();
		
		String host = "neuromancer.eldiosantos.net";
		String user = "eldius";
		Integer port = 80;
		String password = JOptionPane.showInputDialog("Enter password");
		
		Session session=jsch.getSession(user, host, port);
		
		session.setPassword(password);
		
		UserInfo ui = new UserInfo(){
	        public void showMessage(String message){
	          JOptionPane.showMessageDialog(null, message);
	        }
	        public boolean promptYesNo(String message){
	          Object[] options={ "yes", "no" };
	          int foo=JOptionPane.showOptionDialog(null, 
	                                               message,
	                                               "Warning", 
	                                               JOptionPane.DEFAULT_OPTION, 
	                                               JOptionPane.WARNING_MESSAGE,
	                                               null, options, options[0]);
	          return foo==0;
	        }
			@Override
			public String getPassphrase() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public String getPassword() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public boolean promptPassword(String message) {
				// TODO Auto-generated method stub
				return false;
			}
			@Override
			public boolean promptPassphrase(String message) {
				// TODO Auto-generated method stub
				return false;
			}
	 
	        // If password is not given before the invocation of Session#connect(),
	        // implement also following methods,
	        //   * UserInfo#getPassword(),
	        //   * UserInfo#promptPassword(String message) and
	        //   * UIKeyboardInteractive#promptKeyboardInteractive()
	 
	      };
	      
	      session.setUserInfo(ui);
	      
	      Channel channel=session.openChannel("shell");
	      
	      channel.setInputStream(System.in);
	      
	      channel.setOutputStream(System.out);
	      
	      channel.connect();
	}

}
