package net.eldiosantos.auth.service;

import java.util.List;

import net.eldiosantos.auth.ResourceDescriptor;

public interface ResourceDescriptorMapper {
	public void insertResourceDescriptor(ResourceDescriptor ResourceDescriptor);

	public ResourceDescriptor getResourceDescriptorById(Long id);

	public List<ResourceDescriptor> getAllResourceDescriptors();

	public void updateResourceDescriptor(ResourceDescriptor ResourceDescriptor);

	public void deleteResourceDescriptor(Long id);
}
