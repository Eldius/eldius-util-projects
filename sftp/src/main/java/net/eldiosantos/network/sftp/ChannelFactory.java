package net.eldiosantos.network.sftp;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class ChannelFactory {

	private final Session session;

	public ChannelFactory(Session session) {
		super();
		this.session = session;
	}

	public Channel getChannel(String type) throws JSchException{
		Channel channel = session.openChannel(type);
		channel.connect();

		return channel;
	}
}
