package net.eldiosantos.controller;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.http.route.RouteBuilder;
import br.com.caelum.vraptor.http.route.Router;
import br.com.caelum.vraptor.http.route.RoutesConfiguration;
import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.StereotypeHandler;
import br.com.caelum.vraptor.resource.DefaultResourceMethod;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.util.Stringnifier;

/*@Component
@ApplicationScoped*/
public class ResourcesHandler implements StereotypeHandler {

	private static Logger logger = Logger.getLogger(ResourcesHandler.class);

	private Set<Class<?>> resources = new HashSet<Class<?>>();
	private Map<String, ResourceMethod> methods = new HashMap<String, ResourceMethod>();
	
	private Router router;

	public Class<? extends Annotation> stereotype() {
		return Resource.class;
	}

	public void handle(Class<?> type) {
		resources.add(type);
		try {
			Method[] methods = type.getDeclaredMethods();

			for (Method method : methods) {
				if (Modifier.isPublic(method.getModifiers())) {
					StringBuffer uri = new StringBuffer();
					ResourceMethod resourceMethod = DefaultResourceMethod
							.instanceFor(type, method);

					String pathInicial = "/visitacao/";
					uri.append(pathInicial);

					if (type.isAnnotationPresent(Path.class)) {
						String[] value = type.getAnnotation(Path.class).value();
						uri.append(value[value.length - 1]);
					} else {
						String[] tmp = type.getCanonicalName()
								.replace(".", "-").split("-");
						String name = tmp[tmp.length - 1];
						uri.append(name.replace("Controller", "").toLowerCase());
					}

					uri.append("/");

					if (method.isAnnotationPresent(Path.class)) {
						String[] tmp = method.getAnnotation(Path.class).value();
						if (!type.isAnnotationPresent(Path.class)) {
							uri = new StringBuffer(pathInicial);
						}
						uri.append(!tmp[tmp.length - 1].equals("/") ? tmp[tmp.length - 1]
								: "");
					} else {
						uri.append(method.getName());
					}

					try {
						String str = String.format(
								"[Rota: %-60s Metodo: %-100s Class: %s]", uri
										.toString(), Stringnifier
										.simpleNameFor(resourceMethod
												.getMethod()), type.getName());

						logger.debug(str);
					} catch (Exception e) {
						logger.error("Erro ao agrupar resources.", e);
					}
					this.methods.put(uri.toString(), resourceMethod);
				}
			}
		} catch (Exception e) {
			logger.error("Erro ao agrupar resources.", e);
		}
	}

	public Set<Class<?>> allResources() {
		return Collections.unmodifiableSet(resources);
	}

	/**
	 * @return the methods
	 */
	public Map<String, ResourceMethod> getMethods() {
		return methods;
	}
}
