package net.eldiosantos.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.http.route.Route;
import br.com.caelum.vraptor.http.route.Router;

@Resource
public class IndexController {

	private Router router;
	private Result result;

	public IndexController(Router router, Result result) {
		super();
		this.router = router;
		this.result = result;
	}

	@Path("/")
	public void index(){
		List<String>lista=new ArrayList<String>();
		
		
		
		for(Route r:router.allRoutes()){
			System.out.println(r.getOriginalUri().toString());
			lista.add(r.getOriginalUri().toString());
		}
		
		result.include("lista", lista);
	}
}
