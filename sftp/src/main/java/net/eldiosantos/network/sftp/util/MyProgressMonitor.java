package net.eldiosantos.network.sftp.util;

import com.jcraft.jsch.SftpProgressMonitor;

public class MyProgressMonitor implements SftpProgressMonitor{

	@Override
	public void init(int op, String src, String dest, long max) {
		// TODO Auto-generated method stub
//		System.out.println("init...");
//		System.out.println("op: " + op + "src: " + src + "dest: " + dest + "max: " + max);
		
		System.out.println("Starting process...");
		System.out.println("Operation: " + op);
		System.out.println("Src: " + src);
		System.out.println("Dest: " + dest);
		
	}

	@Override
	public boolean count(long count) {
		// TODO Auto-generated method stub
		System.out.println("count...");
		System.out.println("count: " + count);
		return false;
	}

	@Override
	public void end() {
		System.out.println("Finalizing process...");
	}
   
  }
