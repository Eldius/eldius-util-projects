package net.eldiosantos.standaloneserver;

import java.io.FileInputStream;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.xml.XmlConfiguration;


//import org.mortbay.jetty.Connector;
//import org.mortbay.jetty.Server;
//import org.mortbay.jetty.bio.SocketConnector;
//import org.mortbay.jetty.webapp.WebAppContext;

public class Start {

	private static String PROJECT_ROOT = "/dev/workspaces/stok-2.12-branch/stok-2.12/stok-adapter-cga";
	private static String CONFIG_FILE = PROJECT_ROOT + "/src/main/webapp/WEB-INF/jetty-env.xml";
//	public static void main(String[] args) throws Exception {
//		Server server = new Server();
//		SocketConnector connector = new SocketConnector();
//
//		// Set some timeout options to make debugging easier.
//		connector.setMaxIdleTime(1000 * 60 * 60);
//		connector.setSoLingerTime(-1);
//		connector.setPort(8080);
//		server.setConnectors(new Connector[] { connector });
//
//		WebAppContext context = new WebAppContext();
//		context.setServer(server);
//		context.setContextPath("/");
//
//		ProtectionDomain protectionDomain = Start.class.getProtectionDomain();
//		URL location = protectionDomain.getCodeSource().getLocation();
//		context.setWar(location.toExternalForm());
//
//		server.addHandler(context);
//		try {
//			server.start();
//			System.in.read();
//			server.stop();
//			server.join();
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.exit(100);
//		}
//	}

    public static void main(String[] args) throws Exception
    {
    	Server server = new Server(8080);

    	new Thread(new Runnable() {
			
    		private Server server;
    		
    		public Runnable setServer(Server server){
    			this.server = server;
    			return this;
    		}
    		
			@Override
			public void run() {
		        try {
		        	
		            String jetty_home = System.getProperty("jetty.home","..");

		            WebAppContext webapp = new WebAppContext();
		            webapp.setContextPath("/");
		            webapp.setWar(PROJECT_ROOT + "/target/stok-adapter-cga-1.0-SNAPSHOT.war");
//		            webapp.setClassLoader(this.getClass().getClassLoader());

					XmlConfiguration xmlConfiguration = new XmlConfiguration(new FileInputStream(CONFIG_FILE));
					xmlConfiguration.configure(webapp);
		            server.setHandler(webapp);

					server.start();
			        server.join();


				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.setServer(server)).start();

//    	Thread.sleep(1000);
//    	
//        SessionIdManager sessionIdManager = server.getSessionIdManager();
//
//        System.out.println("WorkerName: " + sessionIdManager.getWorkerName());        

    }
}