package net.eldiosantos.controller;

import java.util.List;

import net.eldiosantos.auth.AutentictionConfigurationBean;
import net.eldiosantos.auth.ResourceDescriptor;
import net.eldiosantos.auth.service.ResourceDescriptorService;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.http.route.Router;

@Resource
public class AdminController {

	private Router router;
	private Result result;
	private AutentictionConfigurationBean config;
	private ResourceDescriptorService service;

	public AdminController(Router router, Result result,
			AutentictionConfigurationBean config,
			ResourceDescriptorService service) {
		super();
		this.router = router;
		this.result = result;
		this.config = config;
		this.service = service;
	}

	public void setup() {
		if(config.getResourceDescriptors().isEmpty()){
			List<ResourceDescriptor>configs=service.getAllResourceDescriptors();

			config.setResourceDescriptors(configs);
		}

		result.include("lista", config.getResourceDescriptors());
	}

	public void saveSetup(AutentictionConfigurationBean config){
		
	}
}
