package net.eldiosantos.maven.plugin.integration.server.dsl.interfaces;

import net.eldiosantos.maven.plugin.integration.server.ServerHandler;

public interface ServerHandlerBuilder {

	public abstract ServerHandler build();

}