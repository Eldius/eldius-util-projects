package net.eldiosantos.interceptor;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.eldiosantos.auth.service.ConfigurationService;
import net.eldiosantos.controller.AdminController;
import net.eldiosantos.dao.AuthConfigurationBean;
import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.caelum.vraptor.resource.ResourceMethod;

@Intercepts
@RequestScoped
public class ConfigurationInterceptor implements Interceptor {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private AuthConfigurationBean configurationBean;
	private ConfigurationService service;
	private Result result;


	public ConfigurationInterceptor(AuthConfigurationBean configurationBean, ConfigurationService service, Result result) {
		super();
		this.configurationBean = configurationBean;
		this.service = service;
		this.result = result;
	}

	public void intercept(InterceptorStack stack, ResourceMethod method,
			Object resourceInstance) throws InterceptionException {

		try {
			service.configure();
		} catch (SQLException e) {
			logger.error("Error validating database configuration.", e);
		} catch (ClassNotFoundException e) {
			logger.error("Error validating database configuration.", e);
		}
		
		logger.info("Redirecionando para a página de configuracao de acesso...");
		result.redirectTo(AdminController.class).setup();

	}

	public boolean accepts(ResourceMethod method) {
		boolean intercept = false;

		intercept = (!configurationBean.isConfigured())
				&& (
						(!method.getMethod().getName().equals("setup"))
						&& (!method.getResource().getType().equals(AdminController.class))
					);

		return intercept;
	}

}
