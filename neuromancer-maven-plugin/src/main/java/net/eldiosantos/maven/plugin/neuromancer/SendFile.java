package net.eldiosantos.maven.plugin.neuromancer;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import net.eldiosantos.network.sftp.auth.Credentials;
import net.eldiosantos.network.sftp.auth.HostAccessInfo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo( name = "send", executionStrategy="once-per-session")
public class SendFile extends AbstractMojo
{
    /**
     * Location of the file.
     */
    @Parameter( defaultValue = "${project.build.directory}", property = "outputDir", required = true )
    private File outputDirectory;

    @Parameter(property = "credentials", required = true )
    private Credentials credentials;

    @Parameter(property = "hostAccessInfo", required = true)
    private HostAccessInfo hostAccessInfo;

    public void execute() throws MojoExecutionException
    {

    	System.out.println("Iniciando execução...");
    	File f = new File(outputDirectory.getAbsolutePath() + "/testeFile.txt");

    	if (f.exists()){
    		f.delete();
    	}

    	try {
			f.createNewFile();
	    	PrintWriter print = new PrintWriter(f);

	    	print.append("Editado: " + Calendar.getInstance());
	    	
	    	print.flush();
	    	print.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	System.out.println("Finalizando execução...");
    }
}
