<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
<body>
	<h1>Configuracao de acesso:</h1>
	<form action="<c:url value="/admin/saveSetup" />">
		<fieldset style="width: 350px;">
			<legend>URLs:</legend>
			<c:forEach items="${lista }" var="resource" varStatus="index">
				<p>
					<input type="checkbox"
						value="<c:out value="${resource.id }" default="" />"
						id="resourceId_${index.index }"
						name="resource[${index.index }].id" /> <label
						for="resourceId_${index.index }">
							<c:out value="${resource.resourceUrl }" default="_null_" />
					</label>
				</p>
			</c:forEach>
		</fieldset>

		<fieldset style="width: 350px;">
			<legend>Perfis:</legend>
		</fieldset>

		<fieldset style="width: 350px;">
			<legend>URLs do perfil selecionado:</legend>
			<p>
				<label for="roleName">Perfil: </label><input type="text" name="role.name" id="roleName" />
			</p>
			<p>
				<label for="roles">Perfil: </label>
			</p>
			<p>
				<select multiple="multiple" name="role.name" id="roles" style="width: 200px;">
				</select>
			</p>
		</fieldset>

		<input type="submit" value="Salvar" />
	</form>
</body>
</html>