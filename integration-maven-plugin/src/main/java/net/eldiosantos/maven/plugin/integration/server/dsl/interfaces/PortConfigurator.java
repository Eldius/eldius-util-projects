package net.eldiosantos.maven.plugin.integration.server.dsl.interfaces;

public interface PortConfigurator {

	public abstract ContextPathConfigurator setPort(Integer port);

}