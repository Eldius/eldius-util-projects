package net.eldiosantos.maven.plugin.integration.server.dsl.interfaces;

public interface DatasourceConfigurator {

	public abstract ServerHandlerBuilder setDatasourceFilePath(
			String dataSourceFile);

	public abstract ServerHandlerBuilder skipDatasourceConfiguration();

}