package net.eldiosantos.util.file.tools;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

public class FileDataTransferTest {

	private static final String OUTPUT_FILE_TXT = "target/output-transfer-file.txt";
	private static final String INPUT_FILE_TXT = "arquivo-teste-01.txt";

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public final void testTransferDataToOtherStream() throws IOException {
		
		File inFile = new File(this.getClass().getClassLoader().getResource(INPUT_FILE_TXT).getFile());
		File outFile = new File(OUTPUT_FILE_TXT);

		new FileDataTransfer().transferDataToOtherStream(new FileInputStream(inFile), new FileOutputStream(outFile));

		Scanner in = new Scanner(this.getClass().getClassLoader().getResourceAsStream(INPUT_FILE_TXT));

		Scanner out = new Scanner(new File(OUTPUT_FILE_TXT));

		String inString = new FileContentReader().readFile(in);

		String outString = new FileContentReader().readFile(out);

		assertEquals(inString, outString);
	}

}
