-- CREATE USER 'spring_security'@'%' IDENTIFIED BY 'security123';
-- create database spring_security;
-- GRANT ALL PRIVILEGES ON spring_security.* TO 'spring_security'@'%' WITH GRANT OPTION;

drop database if exists spring_security;

create database spring_security;

use spring_security;

create table perfil (
	idPerfil int not null AUTO_INCREMENT
	, descricao varchar(20) not null
	, CONSTRAINT perfil_pk PRIMARY KEY (idPerfil)
);

create table usuario (
	idUsuario int not null AUTO_INCREMENT
	, nome varchar(30)
	, email varchar(30)
	, username varchar(25) not null unique
	, password varchar(300) not null
	, enabled boolean NOT NULL
	, idPerfil int not null
	, CONSTRAINT usuario_pk PRIMARY KEY (idUsuario)
	, CONSTRAINT fk_usuario_perfil FOREIGN KEY (idPerfil) REFERENCES perfil (idPerfil)
);
