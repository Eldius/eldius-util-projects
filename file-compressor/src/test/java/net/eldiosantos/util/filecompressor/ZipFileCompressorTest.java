package net.eldiosantos.util.filecompressor;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import net.eldiosantos.util.file.tools.FileContentReader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ZipFileCompressorTest {

	private static final String TARGET_COMPRESSED_FILE_TXT_ZIP = "target/compressed.file.txt.zip";
	private static final String TARGET_TEST_DECOMPRESSED_ZIP_FILE_TXT = "target/test-decompressed-zip-file.txt";
	private static final String INPUT_FILE_TXT = "arquivo-teste-01.txt";
	private static final String OUTPUT_FILE_TXT = "target/outputFile.zip";
	private File inFile;

	@Before
	public void setUp() throws Exception {

		File file = new File(OUTPUT_FILE_TXT);
		if (file.exists()) {
			file.delete();
		}

		inFile = new File(this.getClass().getClassLoader().getResource(INPUT_FILE_TXT).getFile());
	}

	@Test
	public final void compressFile() throws IOException {
		new ZipFileCompressor().compressFile(inFile, new File(OUTPUT_FILE_TXT));

		Scanner in = new Scanner(this.getClass().getClassLoader().getResourceAsStream(INPUT_FILE_TXT));

		Scanner out = new Scanner(new File(OUTPUT_FILE_TXT));

		String inString = new FileContentReader().readFile(in);

		String outString = new FileContentReader().readFile(out);

		Assert.assertFalse("Arquivo continua igual.", inString.equals(outString));
	}

	@Test
	public final void decompressFile() throws IOException {

		File compressedFile = new File("target/compressed-txt-file.zip");
		new ZipFileCompressor().compressFile(inFile, compressedFile);

//		Decompress file
		File outputFile = new File("target/");
		new ZipFileCompressor().decompressFile(compressedFile, outputFile);

		assertEquals(
				new FileContentReader().readFile(new Scanner(new File("target/arquivo-teste-01.txt")))
				, new FileContentReader().readFile(new Scanner(inFile))
			);
		
	}
}
