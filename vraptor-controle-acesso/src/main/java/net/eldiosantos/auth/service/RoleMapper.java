package net.eldiosantos.auth.service;

import java.util.List;

import net.eldiosantos.auth.Role;

public interface RoleMapper {
	public void insertRole(Role role);

	public Role getRoleById(Long id);

	public List<Role> getAllRoles();

	public void updateRole(Role role);

	public void deleteRole(Long id);
}
