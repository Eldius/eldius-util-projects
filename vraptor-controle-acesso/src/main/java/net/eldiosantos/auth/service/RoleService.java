package net.eldiosantos.auth.service;

import java.util.List;

import net.eldiosantos.auth.Role;

import org.apache.ibatis.session.SqlSession;

public class RoleService {
	public void insertUser(Role role) {
		SqlSession sqlSession = getSessionFactory();
		try {
			RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
			roleMapper.insertRole(role);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	public Role getRoleById(Long id) {
		SqlSession sqlSession = getSessionFactory();
		try {
			RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
			return roleMapper.getRoleById(id);
		} finally {
			sqlSession.close();
		}
	}

	public List<Role> getAllUsers() {
		SqlSession sqlSession = getSessionFactory();
		try {
			RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
			return roleMapper.getAllRoles();
		} finally {
			sqlSession.close();
		}
	}

	public void updateRole(Role role) {
		SqlSession sqlSession = getSessionFactory();
		try {
			RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
			roleMapper.updateRole(role);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}

	}

	public void deleteUser(Long id) {
		SqlSession sqlSession = getSessionFactory();
		try {
			RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
			roleMapper.deleteRole(id);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	private SqlSession getSessionFactory() {
		return MyBatisUtil.getSqlSessionFactory().openSession();
	}
}
