package net.eldiosantos.util.filecompressor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public interface FileCompressor {

	public abstract void compressFile(File in, File outputFile)
			throws FileNotFoundException, IOException;

	public abstract void decompressFile(File compressedFile, File outputFile)
			throws FileNotFoundException, IOException;

}