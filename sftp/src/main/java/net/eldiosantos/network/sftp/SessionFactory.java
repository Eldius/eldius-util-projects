package net.eldiosantos.network.sftp;

import net.eldiosantos.network.sftp.auth.Credentials;
import net.eldiosantos.network.sftp.auth.HostAccessInfo;
import net.eldiosantos.network.sftp.util.MyUserInfo;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Proxy;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

public class SessionFactory {

	private static Session session;
	private static Credentials credentials;
	private static HostAccessInfo hostAccessInfo;
	
	public SessionFactory(Credentials credentials, HostAccessInfo hostAccessInfo){
		SessionFactory.credentials = credentials;
		SessionFactory.hostAccessInfo = hostAccessInfo;
	}

	public Session getSession() throws JSchException{
		if((session==null)||(!session.isConnected())){
			initSession();
		}

		return session;
	}
	
	private void initSession() throws JSchException{
		prepareSession();

		configureProxy();

		UserInfo ui = new MyUserInfo() {
			public void showMessage(String message) {
				System.out.println("Neuromancer: " + message);
			}

			public boolean promptYesNo(String message) {
				return true;
			}

		};

		session.setUserInfo(ui);

		session.connect(); // making a connection with timeout.
	}

	private void configureProxy() {
		if((hostAccessInfo.getProxyHost()!=null) && (!hostAccessInfo.getProxyHost().equals(""))){
			session.setProxy(getProxy());
		}
	}

	private void prepareSession() throws JSchException {
		JSch jsch = new JSch();

		session = jsch.getSession(credentials.getUser(), hostAccessInfo.getHost(), hostAccessInfo.getPort());
		session.setPassword(credentials.getPassword());
		session.setConfig("StrictHostKeyChecking", "no");
	}

	private Proxy getProxy() {
		// TODO Auto-generated method stub
		ProxyHTTP proxyHTTP = new ProxyHTTP(hostAccessInfo.getProxyHost(), hostAccessInfo.getProxyPort());
		
		proxyHTTP.setUserPasswd(hostAccessInfo.getProxyUser(), hostAccessInfo.getProxyPassword());
		
		return proxyHTTP;
	}
}
