package net.eldiosantos.auth.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

import net.eldiosantos.auth.ResourceDescriptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.caelum.vraptor.http.route.Route;
import br.com.caelum.vraptor.http.route.Router;
import br.com.caelum.vraptor.ioc.Component;

@Component
public class ConfigurationService {

private static final String CREATE_TABLES_SQL_FILE = "create_tables.sql";
/*
	jdbc.driverClassName=com.mysql.jdbc.Driver
	jdbc.url=jdbc:mysql://neuromancer.eldiosantos.net/fichas_rpg_testes
	jdbc.username=ficha-rpg
	jdbc.password=123Senha
*/
	
	private static final String SECURITY_PROPERTIES = "security.properties";
	private static final String JDBC_DRIVER_CLASS = "jdbc.driverClassName";
	private static final String JDBC_URL = "jdbc.url";
	private static final String JDBC_USER = "jdbc.username";
	private static final String JDBC_PASS = "jdbc.password";

	private static Logger logger = LoggerFactory.getLogger(ConfigurationService.class);

	private Router router;
	private ResourceDescriptorService service;

	private Connection connection = null;
	private static final Properties properties;
	
	static{
		Properties props = new Properties();
		
		try {
			props.load(ConfigurationService.class.getClassLoader().getResourceAsStream(SECURITY_PROPERTIES));
		} catch (IOException e) {
			logger.error("Error on file load...", e);
		}
		properties = props;
	}

	public ConfigurationService(Router router, ResourceDescriptorService service) {
		super();
		this.router = router;
		this.service = service;
	}

	public void configure() throws SQLException, ClassNotFoundException{
		String url = properties.getProperty(JDBC_URL);
		String user = properties.getProperty(JDBC_USER);
		String pass = properties.getProperty(JDBC_PASS);
		String clazz = properties.getProperty(JDBC_DRIVER_CLASS);

		Class.forName(clazz);

		connection = DriverManager.getConnection(url, user, pass);
		
		try {
			Statement st = connection.createStatement();
			
			ResultSet rs = st.executeQuery("select * from role");
			
			rs.close();
			st.close();
		}catch (SQLException e) {
			if((connection != null) && (!connection.isClosed())){
				logger.info("Starting to create tables...");
				createTables();
				logger.info("Finish tables creation...");
			}else{
				logger.error("Error stablishing connection with database.", e);
			}
		}

		insertResources();
	}

	public void insertResources(){
		
		ResourceDescriptor rd = null;
		
		for(Route r:router.allRoutes()){
			rd = new ResourceDescriptor();
			rd.setResourceUrl(r.getOriginalUri());

			service.insertResourceDescriptor(rd);
		}
	}

	private void createTables() throws SQLException {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(CREATE_TABLES_SQL_FILE);

		Scanner scanner = new Scanner(in);
		StringBuffer sql = null;
		Statement st = connection.createStatement();
		boolean status;
		
		boolean autocommit = connection.getAutoCommit();
		connection.setAutoCommit(true);
		String tmp = null;

		while(scanner.hasNext()){
			sql = new StringBuffer();
			
			do {
				tmp = scanner.nextLine();
				sql.append(tmp).append("\n");
			} while(!tmp.contains(";"));

			logger.info("Executing...");
			logger.info(sql.toString());

			try {
				status = st.execute(sql.toString());
			} catch (SQLException e) {
				status = false;
				logger.error("Error executing SQL script.", e);
			}

			logger.info("SQL executed: " + status);
		}

		connection.setAutoCommit(autocommit);
	}
}
