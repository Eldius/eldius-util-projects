package net.eldiosantos.auth;

import java.util.Collection;
import java.util.List;

public class Role extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private List<ResourceDescriptor>descriptors;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ResourceDescriptor> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(List<ResourceDescriptor> descriptors) {
		this.descriptors = descriptors;
	}

	public boolean isEmpty() {
		return descriptors.isEmpty();
	}

	public boolean contains(Object o) {
		return descriptors.contains(o);
	}

	public boolean add(ResourceDescriptor e) {
		return descriptors.add(e);
	}

	public boolean remove(Object o) {
		return descriptors.remove(o);
	}

	public boolean containsAll(Collection<?> c) {
		return descriptors.containsAll(c);
	}

	public boolean addAll(Collection<? extends ResourceDescriptor> c) {
		return descriptors.addAll(c);
	}

	public boolean removeAll(Collection<?> c) {
		return descriptors.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return descriptors.retainAll(c);
	}
}
