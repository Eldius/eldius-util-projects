package net.eldiosantos.util.filecompressor;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import net.eldiosantos.util.file.tools.FileContentReader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GZipFileCompressorTest {

	private static final String INPUT_FILE_TXT = "arquivo-teste-01.txt";
	private static final String OUTPUT_FILE_TXT = "target/outputFile.txt";
	private File inFile;

	@Before
	public void setUp() throws Exception {

		File file = new File(OUTPUT_FILE_TXT);
		if (file.exists()) {
			file.delete();
		}

		inFile = new File(this.getClass().getClassLoader().getResource(INPUT_FILE_TXT).getFile());
	}

	@Test
	public final void compressFile() throws IOException {
		new GZipFileCompressor().compressFile(inFile, new File(OUTPUT_FILE_TXT));

		Scanner in = new Scanner(this.getClass().getClassLoader().getResourceAsStream(INPUT_FILE_TXT));

		Scanner out = new Scanner(new File(OUTPUT_FILE_TXT));

		String inString = new FileContentReader().readFile(in);

		String outString = new FileContentReader().readFile(out);

		Assert.assertFalse("Arquivo continua igual.", inString.equals(outString));
	}

	@Test
	public final void decompressFile() throws IOException {

		FileCompressor compressor = new GZipFileCompressor();

		File compressedFile = new File("compressed.file.txt.gz");
		compressor.compressFile(
				new File(compressor.getClass().getClassLoader().getResource("arquivo-teste-01.txt").getFile())
				, compressedFile
			);

		File testDecompressedFile = new File("target/test-decompressed-file.txt");
		compressor.decompressFile(compressedFile, testDecompressedFile);

		assertEquals(
				new FileContentReader().readFile(new Scanner(testDecompressedFile))
				, new FileContentReader().readFile(new Scanner(compressor.getClass().getClassLoader().getResourceAsStream("arquivo-teste-01.txt"))) 
			);
	}
}
