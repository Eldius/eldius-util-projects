package net.eldiosantos.auth;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		boolean equals = false;

		AbstractEntity cmp = (AbstractEntity)obj;

		if (this.id != null) {
			equals = (this.id.equals(cmp.getId()));
		}

		return equals;
	}

	@Override
	public int hashCode() {
		int hash = 0;

		if (this.id != null) {
			hash += this.getId().hashCode();
		}

		return hash;
	}
}
