package net.eldiosantos.auth;

import java.util.List;
import java.util.Vector;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

@Component
@ApplicationScoped
public class AutentictionConfigurationBean {

	private List<ResourceDescriptor> resourceDescriptors = new Vector<ResourceDescriptor>();

	public AutentictionConfigurationBean() {
		
	}
	
	public AutentictionConfigurationBean(List<ResourceDescriptor> resourceDescriptors) {
		super();
		this.resourceDescriptors = resourceDescriptors;
	}

	public List<ResourceDescriptor> getResourceDescriptors() {
		return resourceDescriptors;
	}

	public void setResourceDescriptors(List<ResourceDescriptor> resourceDescriptors) {
		this.resourceDescriptors = resourceDescriptors;
	}
}
