package net.eldiosantos.util.file.tools;

import java.util.Scanner;

public class FileContentReader {

	public String readFile(Scanner out) {
		
		StringBuffer string = new StringBuffer();
		
		while (out.hasNext()) {
			string.append(out.next());
		}
		
		return string.toString();
	}
}
