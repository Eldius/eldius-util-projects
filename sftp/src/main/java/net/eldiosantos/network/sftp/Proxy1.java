package net.eldiosantos.network.sftp;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class Proxy1 {
	
	private boolean useHTTPProxy;
	private boolean useHTTPAuth;
	private String HTTPHost;
	private Integer HTTPPort;
	private String HTTPUsername;
	private String HTTPPassword;
	private String SOCKSHost;
	
	
	
	
	
	
	public String getSOCKSHost() {
		return SOCKSHost;
	}

	public void setSOCKSHost(String sOCKSHost) {
		SOCKSHost = sOCKSHost;
	}

	public String getHTTPPassword() {
		return HTTPPassword;
	}

	public void setHTTPPassword(String hTTPPassword) {
		HTTPPassword = hTTPPassword;
	}

	public String getHTTPUsername() {
		return HTTPUsername;
	}

	public void setHTTPUsername(String hTTPUsername) {
		HTTPUsername = hTTPUsername;
	}

	public Integer getHTTPPort() {
		return HTTPPort;
	}

	public void setHTTPPort(Integer hTTPPort) {
		HTTPPort = hTTPPort;
	}

	public String getHTTPHost() {
		return HTTPHost;
	}

	public void setHTTPHost(String hTTPHost) {
		HTTPHost = hTTPHost;
	}

	public boolean isUseHTTPAuth() {
		return useHTTPAuth;
	}

	public void setUseHTTPAuth(boolean useHTTPAuth) {
		this.useHTTPAuth = useHTTPAuth;
	}

	public boolean isUseHTTPProxy() {
		return useHTTPProxy;
	}

	public void setUseHTTPProxy(boolean useHTTPProxy) {
		this.useHTTPProxy = useHTTPProxy;
	}


	public void setHttpProxy() {
		System.setProperty("proxySet", "true");
		System.setProperty("http.proxyHost", getHTTPHost());
		System.setProperty("http.proxyPort", getHTTPPort().toString());
		System.setProperty("https.proxyHost", getHTTPHost());
		System.setProperty("https.proxyPort", getHTTPPort().toString());
		Authenticator.setDefault(new Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {

		        return new PasswordAuthentication(getHTTPUsername(),getHTTPPassword().toCharArray());
		    }
		});
	}

	public static void main(String[] args) throws IOException {
		Proxy1 p = new Proxy1();
		
		p.setHTTPHost("10.42.0.21");
		p.setHTTPPort(9090);
		p.setHTTPUsername("eldio.junior");
		p.setHTTPPassword("Senha123");
		
		p.setHttpProxy();
		
	}
	
	public class ProxyAuth extends Authenticator {
	    private PasswordAuthentication auth;

	    private ProxyAuth(String user, String password) {
	        auth = new PasswordAuthentication(user, password == null ? new char[]{} : password.toCharArray());
	    }

	    protected PasswordAuthentication getPasswordAuthentication() {
	        return auth;
	    }
	}
}
