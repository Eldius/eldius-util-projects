package net.eldiosantos.auth.service;

import java.util.List;

import net.eldiosantos.auth.ResourceDescriptor;

import org.apache.ibatis.session.SqlSession;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class ResourceDescriptorService {
	public void insertResourceDescriptor(ResourceDescriptor resource) {
		SqlSession sqlSession = getSessionFactory();
		try {
			ResourceDescriptorMapper resourceMapper = sqlSession.getMapper(ResourceDescriptorMapper.class);
			resourceMapper.insertResourceDescriptor(resource);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	public ResourceDescriptor getRoleById(Long id) {
		SqlSession sqlSession = getSessionFactory();
		try {
			ResourceDescriptorMapper resourceMapper = sqlSession.getMapper(ResourceDescriptorMapper.class);
			return resourceMapper.getResourceDescriptorById(id);
		} finally {
			sqlSession.close();
		}
	}

	public List<ResourceDescriptor> getAllResourceDescriptors() {
		SqlSession sqlSession = getSessionFactory();
		try {
			ResourceDescriptorMapper resourceMapper = sqlSession.getMapper(ResourceDescriptorMapper.class);
			return resourceMapper.getAllResourceDescriptors();
		} finally {
			sqlSession.close();
		}
	}

	public void updateResourceDescriptor(ResourceDescriptor resource) {
		SqlSession sqlSession = getSessionFactory();
		try {
			ResourceDescriptorMapper resourceMapper = sqlSession.getMapper(ResourceDescriptorMapper.class);
			resourceMapper.updateResourceDescriptor(resource);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}

	}

	public void deleteResourceDescriptor(Long id) {
		SqlSession sqlSession = getSessionFactory();
		try {
			ResourceDescriptorMapper resourceMapper = sqlSession.getMapper(ResourceDescriptorMapper.class);
			resourceMapper.deleteResourceDescriptor(id);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	private SqlSession getSessionFactory() {
		return MyBatisUtil.getSqlSessionFactory().openSession();
	}
}
