package net.eldiosantos.network.sftp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import net.eldiosantos.network.sftp.util.MyUserInfo;
import net.eldiosantos.network.sftp.util.NewProgressMonitor;
import net.eldiosantos.util.filecompressor.FileCompressor;
import net.eldiosantos.util.filecompressor.ZipFileCompressor;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Proxy;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.ProxySOCKS4;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

public class Shell {
	public static void main(String[] arg) {

		try {
			JSch jsch = new JSch();

			// jsch.setKnownHosts("/home/foo/.ssh/known_hosts");

			String host = null;
//			if (arg.length > 0) {
//				host = arg[0];
//			} else {
//				host = JOptionPane.showInputDialog("Enter username@hostname",
//						"eldius" + "@neuromancer.eldiosantos.net");
//			}
//			String user = host.substring(0, host.indexOf('@'));
//			host = host.substring(host.indexOf('@') + 1);
			
			String user = "eldius";
			host = "neuromancer.eldiosantos.net";

			Session session = jsch.getSession(user, host, 80);

			Proxy proxy = null;
			
			proxy = getProxy();
			session.setProxy(proxy);
//			String passwd = getPassword();
			
			String passwd = "dudusan";
			
			session.setPassword(passwd);
			session.setConfig("StrictHostKeyChecking", "no");

			UserInfo ui = new MyUserInfo() {
				public void showMessage(String message) {
					JOptionPane.showMessageDialog(null, message);
				}

				public boolean promptYesNo(String message) {
					return true;
				}

			};

			session.setUserInfo(ui);

			session.connect(); // making a connection with timeout.

			sftp(session);
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sftp(Session session) throws JSchException, SftpException, FileNotFoundException, IOException {
		
		System.out.println("Iniciando sessao SFTP...");
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		ChannelSftp c = (ChannelSftp) channel;

//		c.setOutputStream(System.out);
		
//		c.ls(".");
		
		File path = new File("");
		String localPath = "C:/Users/eldio.junior/Desktop/dev/eldius-util-projects/";
		
		String remotePath = c.getHome() + "/testes-sftp";
		
		
		
//		c.rmdir(remotePath);
//		c.mkdir(remotePath);

		
		System.out.println("Executando comando...");

		System.out.println("Remotepath: " + remotePath);
		System.out.println("Lista de pastas: ");
		for(Object s:c.ls(".")){
			System.out.println("\t" + s);
		}

		System.out.println(path);

		FileCompressor compressor = new ZipFileCompressor();
		String tmpFilePath = path.getAbsolutePath() + "/../../tmp.zip";
		compressor.compressFile(new File(localPath), new File(tmpFilePath));
		
		c.put(tmpFilePath, remotePath, new NewProgressMonitor(), ChannelSftp.OVERWRITE);
		
		System.out.println();

		System.out.println("Finalizando...");

//		c.put("C:/Users/eldio.junior/Desktop/dev/eldius-util-projects/*", "~/eldius-util-projects/");
		
		System.out.println(path.getAbsolutePath());
		
		c.disconnect();

		session.disconnect();
		
		System.exit(0);
	}
	
	public static void ssh(Session session) throws JSchException{

		Channel ssh = session.openChannel("shell");

		// Enable agent-forwarding.
		// ((ChannelShell)channel).setAgentForwarding(true);

		// channel.setInputStream(System.in);
		/*
		 * // a hack for MS-DOS prompt on Windows.
		 * channel.setInputStream(new FilterInputStream(System.in){ public
		 * int read(byte[] b, int off, int len)throws IOException{ return
		 * in.read(b, off, (len>1024?1024:len)); } });
		 */

//		ssh.setInputStream(new FilterInputStream(System.in) {
//			public int read(byte[] b, int off, int len) throws IOException {
//				return in.read(b, off, (len > 1024 ? 1024 : len));
//			}
//		});
//
//		ssh.setInputStream(System.in);
//		ssh.setOutputStream(System.out);

		/*
		 * // Choose the pty-type "vt102".
		 * ((ChannelShell)channel).setPtyType("vt102");
		 */

		/*
		 * // Set environment variable "LANG" as "ja_JP.eucJP".
		 * ((ChannelShell)channel).setEnv("LANG", "ja_JP.eucJP");
		 */

		// channel.connect();
		ssh.connect(3 * 1000);
	}
	
	private static String getPassword() {
		// Cria campo onde o usuario entra com a senha
		JPasswordField password = new JPasswordField(10);
		password.setEchoChar('*');

		// Cria um rótulo para o campo
		JLabel rotulo = new JLabel("Entre com a senha:");

		// Coloca o rótulo e a caixa de entrada numa JPanel:
		JPanel entUsuario = new JPanel();
		entUsuario.add(rotulo);
		entUsuario.add(password);

		// Mostra o rótulo e a caixa de entrada de password para o usuario
		// fornecer a senha:
		JOptionPane.showMessageDialog(null, entUsuario, "Acesso restrito",
				JOptionPane.PLAIN_MESSAGE);

		return String.copyValueOf(password.getPassword());
	}

	public static Proxy getProxy(){
		ProxyHTTP httpProxy = new ProxyHTTP("10.42.0.21", 9090);
		
		httpProxy.setUserPasswd("eldio.junior", "Senha123");
		
		Proxy socksProxy = new ProxySOCKS4("127.0.0.1", 9595);
		
		return httpProxy;
	}
}