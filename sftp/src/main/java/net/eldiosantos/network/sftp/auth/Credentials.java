package net.eldiosantos.network.sftp.auth;

public class Credentials {

	private final String user;
	private final String password;

	public Credentials(String user, String password) {
		super();
		this.user = user;
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}
}
