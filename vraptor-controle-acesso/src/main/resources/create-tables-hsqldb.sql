create database 'security';

create table role (
	role_id int not null identity
	, name varchar(50) not null unique
);

create table resource_descriptor (
	resource_id int not null identity
	, url varchar(50) not null unique
);

create table role_resouece (
	resource_id int not null
	, role_id int not null
	, primary key (resource_id, role_id)
	, FOREIGN KEY (resource_id) references resource_descriptor (resource_id)
	, FOREIGN KEY (role_id) references role (role_id)
);

