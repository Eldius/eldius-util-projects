package net.eldiosantos.util.filecompressor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import net.eldiosantos.util.file.tools.FileDataTransfer;

public class ZipFileCompressor implements FileCompressor {

	private List<String> fileList;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		FileCompressor compressor = new ZipFileCompressor();
		
		compressor.compressFile(new File("src/test/resources/arquivo-teste-01.txt"), new File("cpmpressed-txt-file.zip"));
		
		compressor.decompressFile(new File("cpmpressed-txt-file.zip"), new File("."));
	}
	
	public OutputStream transferDataToOtherStream(InputStream in,
			OutputStream out) throws IOException {

		byte[] data = new byte[1024];

		int bytes;

		while ((bytes = in.read(data, 0, 1000)) != -1) {
			out.write(data, 0, bytes);
		}

		return out;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.eldiosantos.util.filecompressor.FileCompressor#decompressFile(java
	 * .io.File, java.io.File)
	 */
	public void decompressFile(File compressedFile, File outputFile)
			throws FileNotFoundException, IOException {

		if (!outputFile.exists()) {
			outputFile.mkdir();
		}

		ZipInputStream zis = new ZipInputStream(new FileInputStream(
				compressedFile));

		ZipEntry ze = zis.getNextEntry();

		while (ze != null) {
			String fileName = ze.getName();
			File newFile = new File(outputFile.getAbsolutePath() + File.separator + fileName);

			if(!newFile.exists()){
				new File(newFile.getParent()).mkdirs();
			}

			FileOutputStream fos = new FileOutputStream(newFile);

			byte[] buffer = new byte[1024];
			int len;

			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}

			fos.flush();
			fos.close();
			ze = zis.getNextEntry();
		}
		
	}

	public void compressFile(File in, File outputFile)
			throws FileNotFoundException, IOException {

		this.fileList = new Vector<String>();

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
				outputFile));
		File rootPath = null;
		if (in.isDirectory()) {
			rootPath = in;
		} else if (in.isFile()) {
			rootPath = in.getAbsoluteFile().getParentFile();
		}

		generateFileList(in, rootPath);

		for (String file : this.fileList) {

			addZipEntry(out, file);

			FileInputStream fis = new FileInputStream(rootPath
					.getAbsoluteFile().toString() + File.separator + file);

			new FileDataTransfer().transferDataToOtherStream(fis, out);

			fis.close();
		}
		out.flush();
		out.close();
	}

	private void addZipEntry(ZipOutputStream out, String file)
			throws IOException {
		ZipEntry ze = new ZipEntry(file);
		out.putNextEntry(ze);
	}

	public void generateFileList(File node, File rootPath) {

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(new File(node, filename), rootPath);
			}
		} else if (node.isFile()) {
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString(),
					rootPath));
		}
	}

	private String generateZipEntry(String file, File rootPath) {
		return file.substring(
				rootPath.getAbsolutePath().toString().length() + 1,
				file.length());
	}
}
