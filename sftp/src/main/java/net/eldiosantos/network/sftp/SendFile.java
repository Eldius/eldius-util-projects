package net.eldiosantos.network.sftp;

import java.io.File;

import net.eldiosantos.network.sftp.util.NewProgressMonitor;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SendFile {

	private final Session session;

	public SendFile(Session session) {
		super();
		this.session = session;
	}

	public void send(String src, String dest) throws JSchException, SftpException {

		ChannelSftp channel = (ChannelSftp) new ChannelFactory(session).getChannel("sftp");
		
		File file = new File(src);
		
		if(file.isFile()){
			channel.put(src, dest, new NewProgressMonitor());
		}
	}
}
