package net.eldiosantos.dao;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

@Component
@ApplicationScoped
public class AuthConfigurationBean {

	private boolean configured = false;

	public AuthConfigurationBean() {
		super();
	}

	public boolean isConfigured() {
		return configured;
	}

	public void setConfigured(boolean configured) {
		this.configured = configured;
	}
}
