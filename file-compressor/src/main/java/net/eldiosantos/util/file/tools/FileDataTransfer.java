package net.eldiosantos.util.file.tools;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileDataTransfer {


	public OutputStream transferDataToOtherStream(InputStream in,
			OutputStream out) throws IOException {

		byte[] data = new byte[1024];

		int bytes;

		while ((bytes = in.read(data, 0, 1000)) != -1) {
			out.write(data, 0, bytes);
		}

		return out;
	}
}
